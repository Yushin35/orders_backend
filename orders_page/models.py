from django.db import models
from django.utils import timezone

# Create your models here.


class Advert(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    login = models.CharField(max_length=255)
    password = models.CharField(max_length=255)


class Good(models.Model):
    name = models.CharField(max_length=255)
    price = models.FloatField()
    advert = models.ForeignKey(Advert, on_delete=models.CASCADE)


class State(models.Model):
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)


class Order(models.Model):
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    good = models.ForeignKey(Good, on_delete=models.CASCADE)
    client_phone = models.CharField(max_length=255)
    client_name = models.CharField(max_length=255)
    add_time = models.DateTimeField(default=timezone.now)

