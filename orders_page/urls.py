from django.conf.urls import url
from django.urls import include, path

from . import views


urlpatterns = [
    url(r'^orders_page', views.orders_page, name='orders_page'),
    url(r'^get_orders', views.get_orders, name='get_orders')
]