from faker import Faker
from .models import Order, State, Advert, Good



# TODO goods_ids

fake = Faker()


def state_factory():
    states = {
        'new': 'Новый',
        'on_operate': 'В работе',
        'accepted': 'Подтвержден'
    }
    for slug, name  in states.items():
        State.objects.create(slug=slug, name=name)


def order_factory(count=15):
    good_ids = [good_id[0] for good_id in Good.objects.all().values_list('pk')]
    state_ids = [state_id[0] for state_id in State.objects.all().values_list('pk')]
    if good_ids:
        for i in range(1, count + 1):
            Order.objects.create(client_phone=fake.phone_number(),
                                 client_name=fake.name(),
                                 add_time=fake.date_time(),
                                 good=Good.objects.get(pk=fake.random_element(good_ids)),
                                 state=State.objects.get(pk=fake.random_element(state_ids)))


def advert_factory(count=10):
    for i in range(1, count + 1):
        Advert.objects.create(first_name=fake.first_name(),
                              last_name=fake.last_name(),
                              login=fake.user_name(),
                              password=fake.password())


def good_factory(count=15):
    advert_ids = [ad_id[0] for ad_id in Advert.objects.all().values_list('pk')]
    if advert_ids:
        for i in range(1, count + 1):
            Good.objects.create(name=fake.name(),
                                price=float(fake.random.randint(0, 10*5)),
                                advert=Advert.objects.get(pk=fake.random_element(advert_ids)))


def fill_database():
    state_factory()
    advert_factory(1000)
    good_factory(1000)
    order_factory(1000)


def clear_databases():
    State.objects.all().delete()
    Advert.objects.all().delete()
    Good.objects.all().delete()
    Order.objects.all().delete()
