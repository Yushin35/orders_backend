function cookie_(cook) {
  var data = document.cookie.split('=');
  var index = data.indexOf(cook);
  return data[index+1]
}

$(document).ready(function() {
    $.ajaxSetup({
        headers: { "X-CSRFToken": cookie_('csrftoken') }
    });
    $('#send').on('click', getOrders);
    table = $('#ordersTable').DataTable();
    getOrders();
} );


function getOrders() {
    console.log('get orders');
    var data = $('.data').serialize();
    $.ajax(
        {
           type: 'GET',
            url: '/get_orders',
            dataType: 'json',
            data: data,
            success: function (data) {
                console.log(data);
                table.clear().draw();
                $.each(data, function (index, value) {

                    table.row.add(
                        [/*
                            value.id,
                            value.add_time ,
                            value.client_name,
                            value.client_phone,
                            value.good_name+ "<br>("+value.good_advert_first_name +" "
                            +value.good_advert_last_name+")"+"<br>"+value.good_advert_login
                            ,
                            value.state_name
                            */
                            value.id,
                            value.add_time,
                            value.client_name,
                            value.client_phone,
                            value.good__name+ "<br>("+value.good__advert__first_name +" "
                            +value.good__advert__last_name+")"+"<br>"+value.good__advert__login
                            ,
                            value.state__name
                        ]
                    ).draw();
                });
            },
            error: function (data) {
            console.log('error');
                console.log(data);
            }
        }
    );

}
