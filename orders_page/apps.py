from django.apps import AppConfig


class OrdersPageConfig(AppConfig):
    name = 'orders_page'
