from django.shortcuts import render
from django.http import HttpResponse
from .models import State, Order, Good, Advert
import json, time
from django.core import serializers
# Create your views here.

from datetime import date, datetime


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    s = '+00:00'
    if isinstance(obj, (datetime, date)):
        return str(obj)[:-len(s)]
    raise TypeError("Type %s not serializable" % type(obj))


def orders_page(request):
    return render(request, 'orders.html')


def get_orders(request):
    args = ['id', 'client_name', 'client_phone', 'add_time',
            'good__name', 'good__advert__first_name',
            'good__advert__last_name', 'good__advert__login',
            'state__name']
    sep = {'startDate': 'add_time__gte', 'endDate': 'add_time__lte',
           'state': 'state__slug', 'id': 'pk',
           'phone': 'client_phone__contains',
           'good': 'good__name__contains'}
    filter_data = request.GET.copy()
    q = Order.objects.all()
    for key, value in filter_data.items():
        if value and value != 'all':
            q = q.filter(**{sep[key]: value})
    json_data = json.dumps(list(q.values(*args)), default=json_serial)
    return HttpResponse(content=json_data)
